#!/bin/bash

#####################################################################
# recherche et élimination récursive de doublons dans un répertoire #
# syntaxe : double.sh source dbfile                                 #
# source = répertoire à traiter                                     #
# dbfile = fichier où est enregistré les signatures des fichiers    #
#####################################################################

BEFORE=$(date  +%s)
echo "BEFORE : $BEFORE "

#nom du fichier contenant les fichiers supprimés
EFFLIST="efflist.txt" 

# récupération des arguments de la ligne de commande
SRC=$1
DB=$2

DEBNB=`find $SRC -type f |wc -l`
DEBSIZE=`du -h --max-depth=0  ../randomFolders/results/`

echo Le répertoire à analyser est : $SRC
echo Le fichier de donnée est : $DB

echo "Suppression des fichiers temporaires"
rm -f $DB $EFFLIST

echo "recherche récursive de fichiers"

find $1 -type f | while read file ; do
	MD5L=$(sha1sum $file)
	MD5C=$(echo $MD5L |cut -d " " -f 1)
	#recherche de la signature
	grep $MD5C $DB

	if [ $? -eq 0 ]
	then
		echo "md5sum trouvé" 
		#rm $file
		echo "fichier $file supprimé" >> $EFFLIST
	else
		#md5 non trouvé, ajout à la bdd
		echo $MD5L >> $DB
	fi

done

FINBNB=`find $SRC -type f |wc -l`
FINSIZE=`du -h --max-depth=0  ../randomFolders/results/`

AFTER=$(date  +%s)
echo "AFTER : $AFTER"
INTDUREE=`expr $(( ($AFTER-$BEFORE) ))`

NBEFF=`cat $EFFLIST |wc -l`

echo "Avant analyse : $DEBNB fichiers pour une taille de $DEBSIZE"
echo "Après analyse : $FINNB fichiers pour une taille de $FINSIZE"
echo "$NBEFF fichiers ont été éffacés"
echo "Durée de l'analyse : $INTDUREE secondes"
